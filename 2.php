<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 2</title>
</head>

<body>

    <h1>Ejercicio 2 de la practica 1</h1>

    <table width="100%" border="1">
        <tr>
            <td>
                <?php
                echo "Este texto esta escrito utilizando la funcion echo de PHP";
                ?>
            </td>
            <td>
                Este texto está escrito en HTML
            </td>
        </tr>
        <tr>
            <td>
                <?php
                print "Este texto esta escrito desde PHP con la función print";
                ?>
            </td>
            <td>
                Centro de Formación Alpe
            </td>
        </tr>
    </table>

</body>

</html>