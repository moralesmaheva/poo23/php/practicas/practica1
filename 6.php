<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 6</title>

</head>

<body>

    <?php

    echo "Quiero que coloque este texto en pantalla" . '<p align="center">' . "Academia Alpe" . "</p>";

    ?>

    <h2>Puedo colocar todo en el mismo echo</h2>

    <?php 
    echo 'Quiero que coloque este texto en pantalla <p align="center"> Academia Alpe </p>';
    ?>
</body>

</html>