<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 1</title>
</head>

<body>

    <div>Es texto escrito directamente en HTML</div>

    <?php
    //Comentario de linea
    echo "texto escrito desde PHP <br>";

    /* 
Comentario de varias lineas
*/
    print "Texto escrito desde PHP";

    ?>

    <h1>Segunda parte</h1>

    <?php
    #Comentario de una linea
    $a = 10;
    echo "Escribir en PHP para colocar el valor de la variable $a";
    ?>

</body>

</html>