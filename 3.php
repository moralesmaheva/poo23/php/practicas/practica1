<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 3</title>
</head>

<body>

    <h2>Metodo 1</h2>
    <?php
    //mezclamos HTML y PHP
    echo "<p> Hola mundo </p>";
    ?>

    <h2>Metodo 2</h2>

    <p>
        <?php
        //solo coloco php
        echo "Hola mundo";
        ?>
    </p>

    <h2>Metodo 3</h2>

    <?php
    echo "<p>";
    ?>
    Hola mundo
    <?php
    echo "</p>";
    ?>

    <h2>Podemos utilizar el modo contraido del echo</h2>

    <p>
        <?= "hola mundo" ?>
    </p>
</body>

</html>